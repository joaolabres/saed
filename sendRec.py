import serial
import time

ser3 = serial.Serial(
    port='/dev/tnt0',
    baudrate=9600,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.EIGHTBITS
)
ser2 = serial.Serial(
    port='/dev/tnt1',
    baudrate=9600,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.EIGHTBITS
)
# ser2.readable()
# ser3.writable()
send = []
send.append("100.0;89.5;89.5;89.0;89.5;70.5\n")
send.append("85.0;85.5;85.5;85.0;85.5;85.5\n")
send.append("78.0;78.5;78.5;78.0;78.5;78.5\n")
send.append("70.0;70.5;70.5;70.0;70.5;70.5\n")
send.append("62.0;62.5;62.5;62.0;62.5;62.5\n")
send.append("54.0;54.5;54.5;54.0;54.5;54.5\n")
send.append("50.0;50.5;50.5;50.0;50.5;50.5\n")
send.append("44.0;44.5;44.5;44.0;44.5;44.5\n")
send.append("40.0;40.5;40.5;40.0;40.5;40.5\n")
send.append("36.0;36.5;36.5;36.0;36.5;36.5\n")
send.append("32.0;32.5;32.5;32.0;32.5;32.5\n")
send.append("29.0;29.5;29.5;29.0;29.5;29.5\n")
send.append("25.0;25.5;25.5;25.0;25.5;25.5\n")
send.append("20.0;20.5;20.5;20.0;20.5;20.5\n")
send.append("18.0;18.5;18.5;18.0;18.5;18.5\n")
send.append("17.0;17.5;17.5;17.0;17.5;17.5\n")
send.append("15.0;15.5;15.5;15.0;15.5;15.5\n")
send.append("11.0;11.5;11.5;11.0;11.5;11.5\n")
send.append("11.0;11.5;11.5;11.0;11.5;11.5\n")
i = 0
while(1):
    reading = ser3.readline().decode()
    print(reading)
    if(reading == "B1\n"):
        ser3.write(send[i].encode())
        print(send[i].encode())
        ser3.flushInput()
        if(i < len(send)-1):
            i += 1
        ser3.close()
        time.sleep(.2)
        ser3.open()


# while(1):z
    # send = input()
