#define INF -1123456
char uart_rd[15];
char voltageTxt[15];
const unsigned short VREF = 5;

float convert(int inputSensor);
float temperature(float pos, float neg);

float convert(int inputSensor){
  float voltage;
  int ADCResult = 0;
  ADC_Init();
  ADCResult = ADC_Get_Sample(inputSensor);       // Get 10-bit results of AD conversion
  voltage = VREF/(1024.0);  // Read the voltage
  voltage *= ADCResult*100;
  return voltage;
}

float temperature(int pos, int neg){
  float res = 0;
  float result1, result2, result3;
  LATD2_bit = 0b00000001;
  result1 = convert(pos);
  result2 = convert(neg);
  LATD2_bit = 0b00000000;
  Delay_100ms();
  LATD2_bit = 0b00000001;
  Delay_100ms();
  LATD2_bit = 0b00000000;
  if(result1 < -10)
    result1 += 320;
  if(result2 > result1){
    result1 *= -1;
//    result1 = modf(result1, 60);
//    res -= 0.5;
  }
  res += result1 - result2;
//  if(res == 0.5) res = 0;
  return (res);
}

void main() {
  char send[100];
  int i, j;
  TRISD = 0;
  ADCON1 = 0x30;                     // Configure A/D for digital inputs
  TRISB7_bit = 0;
  CMCON  = 0x07;                     // Configure comparators for digital input
  UART1_Init(9600);                  // Initialize UART module at 9600 bps
  Delay_ms(1000);                  // Wait for UART module to stabilize
  LATD3_bit = 1;
  while (1) {                     // Endless loop
    Delay_ms(400);
    LATD2_bit = 0b00000000;
    memset(uart_rd, 0, sizeof uart_rd);
    memset(send, 0, sizeof send);
    LATB7_bit = 0;
    i = 0;
    while(UART_Data_Ready()){
      LATD2_bit = 0b00000001;
      uart_rd[i++] = UART_Read();     // read the received data,
      LATD2_bit = 0b00000000;
    }
    if(!strcmp(uart_rd, "B1")){
      LATB7_bit = 0;
      for(j = 0; j < 6; j++){
         FloatToStr(temperature(j*2, (j*2)+1), voltageTxt);    // Convert voltage to string
         strcat(send, voltageTxt);
         strcat(send, ";");
      }
      strcat(send, "\n");
      LATD2_bit = 0b00000001;
      UART1_Write_Text(send);
      LATB7_bit = 0;
    }
    Delay_ms(400);
  }
  LATD3_bit = 0;
}